package com.isaac.mobovivo_internship.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.isaac.mobovivo_internship.twitter.TweetImage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class GridViewImageAdapter extends BaseAdapter {

	private Activity mActivity;
	private ArrayList<TweetImage> mTweetList = new ArrayList<TweetImage>();
	private int mImageWidth;

	public GridViewImageAdapter(Activity activity,
			ArrayList<TweetImage> tweetList, int imageWidth) {
		this.mActivity = activity;
		this.mTweetList = tweetList;
		this.mImageWidth = imageWidth;
	}

	@Override
	public int getCount() {
		return this.mTweetList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.mTweetList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {
			imageView = new ImageView(mActivity);
		} else {
			imageView = (ImageView) convertView;
		}

		Bitmap image = decodeFile(mTweetList.get(position).getImagePath(),
				mImageWidth, mImageWidth);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setLayoutParams(new GridView.LayoutParams(mImageWidth, mImageWidth));
		imageView.setImageBitmap(image);
		imageView.setBackgroundResource(android.R.drawable.list_selector_background);
		return imageView;
	}

	public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
		try {
			File f = new File(filePath);

			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			int scale = 1;
			while (o.outWidth / scale / 2 >= WIDTH && o.outHeight / scale / 2 >= HIGHT)
				scale *= 2;

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}