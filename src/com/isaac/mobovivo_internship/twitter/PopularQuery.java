package com.isaac.mobovivo_internship.twitter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import twitter4j.MediaEntity;
import twitter4j.Query;
import twitter4j.Query.ResultType;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.util.Log;

import com.isaac.mobovivo_internship.AppConstants;
import com.isaac.mobovivo_internship.util.Utils;

public class PopularQuery {
//	TODO set favorite and retweet thresholds
//	because aparently the 'popular query is limited to 15 tweets
	private static final String TAG = "PopularQuery";

	private static final String[] related_words = new String[] { "\'world cup\'", "soccer", "football -american" };
	protected Twitter mTwitter;
	private ResultType mResultType = Query.ResultType.popular;
	private String mLang = "en";
	private int mCount = 100;

	public PopularQuery() {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
			.setOAuthConsumerKey(AppConstants.CONSUMER_KEY)
			.setOAuthConsumerSecret(AppConstants.CONSUMER_SECRET)
			.setOAuthAccessToken(AppConstants.ACCESS_TOKEN)
			.setOAuthAccessTokenSecret(AppConstants.ACCESS_TOKEN_SECRET);
		mTwitter = new TwitterFactory(cb.build()).getInstance();
	}

	public List<TweetImage> search(int count) throws TwitterException {
		List<TweetImage> result = new ArrayList<TweetImage>();

		List<Status> tweetsList = new ArrayList<Status>();
		for (int i = 0; i < related_words.length; i++) {
			Query query = new Query("pic.twitter.com " + related_words[i]);
			query.resultType(mResultType);
			query.setLang(mLang);
			query.count(mCount);

			List<Status> tweets = mTwitter.search(query).getTweets();
			Log.d(TAG, query.getQuery() + "; Count: " + tweets.size());
			tweets.get(0).getFavoriteCount();
			tweetsList.addAll(tweets);
		}

	    Random random = new Random();
	    while (count > 0 && tweetsList.size() > 0) {
	    	int i = random.nextInt(tweetsList.size());
	    	Status tweet = tweetsList.get(i);
	    	String tweetText = "@" + tweet.getUser().getScreenName() + " - " + tweet.getText();
	    	try {
				MediaEntity[] medias = tweet.getMediaEntities();
				String imageUrl = medias[0].getMediaURL();
				if (Utils.IsSupportedFile(imageUrl)) {
					long id = tweet.getId();
					boolean found = false;
					for (TweetImage ti : result) {
						if (ti.getId() == id) {
							found = true;
							break;
						}
					}
					if (!found) {
						result.add(new TweetImage(tweet.getId(), tweetText, imageUrl));
						tweetsList.remove(i);
				    	count--;
					}
				}
	    	} catch (Exception e) {
	    		// Do nothing, media is not image
	    	}
	    }

		return result;
	}

	public int getCount() {
		return mCount;
	}

	public void setCount(int mCount) {
		this.mCount = mCount;
	}

	public String getLang() {
		return mLang;
	}

	public void setLang(String mLang) {
		this.mLang = mLang;
	}

	public ResultType getResultType() {
		return mResultType;
	}

	public void setResultType(ResultType mResultType) {
		this.mResultType = mResultType;
	}
}
