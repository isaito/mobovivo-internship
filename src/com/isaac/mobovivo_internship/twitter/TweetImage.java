package com.isaac.mobovivo_internship.twitter;

public class TweetImage {

	private long mId;
	private String mTweet;
	private String mImageUrl;
	private String mImagePath;

	public TweetImage(long l, String tweet, String imgeUrl) {
		setId(l);
		setTweet(tweet);
		setImageUrl(imgeUrl);
	}

	public long getId() {
		return mId;
	}

	public void setId(long l) {
		this.mId = l;
	}

	public String getTweet() {
		return mTweet;
	}

	public void setTweet(String tweet) {
		this.mTweet = tweet;
	}

	public String getImageUrl() {
		return mImageUrl;
	}

	public void setImageUrl(String imgeUrl) {
		this.mImageUrl = imgeUrl;
	}

	public String getImagePath() {
		return mImagePath;
	}

	public void setImagePath(String mImagePath) {
		this.mImagePath = mImagePath;
	}

}
