package com.isaac.mobovivo_internship.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
 

import com.isaac.mobovivo_internship.AppConstants;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
 
public class Utils {
 
	private static String TAG = "Utils";
	
    private Context mContext;

    public Utils(Context context) {
        this.mContext = context;
    }

    public ArrayList<String> getFilePaths() {
        ArrayList<String> filePaths = new ArrayList<String>();
        File directory = new File(mContext.getFilesDir().getAbsolutePath() + '/' + AppConstants.DATA_DIR);
        if (directory.isDirectory()) {
            File[] listFiles = directory.listFiles();
            if (listFiles.length > 0) {
                for (int i = 0; i < listFiles.length; i++) {
                    String filePath = listFiles[i].getAbsolutePath();
                    if (IsSupportedFile(filePath)) {
                        filePaths.add(filePath);
                    }
                }
            } else {
            	Log.e(TAG, "The " + directory.getAbsoluteFile() + " is empity");
            }
        } else {
        	Log.e(TAG, "The " + directory.getAbsoluteFile() + " is not valid (it is not a directory)");
        }
        return filePaths;
    }

    public static boolean IsSupportedFile(String filePath) {
        String ext = filePath.substring((filePath.lastIndexOf(".") + 1),
                filePath.length());
        if (AppConstants.FILE_EXTN.contains(ext.toLowerCase(Locale.getDefault())))
            return true;
        else
            return false;
    }

	public int getScreenWidth() {
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (java.lang.NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		if (point.x > point.y) {
			return point.y;
		} else {
			return point.x;
		}
	}
}