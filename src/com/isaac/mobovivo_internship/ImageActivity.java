package com.isaac.mobovivo_internship;

import java.io.File;

import com.isaac.mobovivo_internship.R;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageActivity extends Activity {

	protected String TAG = "ImageActivity";

	protected TextView mTextView;
	protected ImageView mImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);
		mTextView = (TextView) findViewById(R.id.txtTwitter);
		mImageView = (ImageView) findViewById(R.id.imgTwitter);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mTextView.setText(extras.getString("text"));
			mImageView.setImageURI(Uri.fromFile(new File(extras.getString("image"))));
		}
	}
}