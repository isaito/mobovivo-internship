package com.isaac.mobovivo_internship;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import twitter4j.TwitterException;

import com.isaac.mobovivo_internship.R;
import com.isaac.mobovivo_internship.adapter.GridViewImageAdapter;
import com.isaac.mobovivo_internship.twitter.TweetImage;
import com.isaac.mobovivo_internship.twitter.PopularQuery;
import com.isaac.mobovivo_internship.util.Utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	protected String TAG = "MainActivity";

	protected Utils mUtils;
    protected ArrayList<TweetImage> mTweetList = new ArrayList<TweetImage>();
	protected GridViewImageAdapter mAdapter;
	protected GridView mGridView;
	protected int mColumnWidth;
	protected String mDataPath;

	protected static final int MAX_Q_PAGE = 100;
	
	protected TextView txtInfo;
	protected TextView txtProgress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDataPath = getFilesDir().getAbsolutePath() + '/' + AppConstants.DATA_DIR;
		try {
			FileUtils.deleteDirectory(new File(mDataPath));
		} catch (IOException e) {
			Log.e(TAG, "It was not possible to remove previous files");
		}
		new File(mDataPath).mkdirs();

		setContentView(R.layout.activity_main);
		mGridView = (GridView) findViewById(R.id.grid_view);
		txtInfo = (TextView) findViewById(R.id.txt_info);
		txtProgress = (TextView) findViewById(R.id.txt_progress);
		mUtils = new Utils(this);

		InitilizeGridLayout();
		mAdapter = new GridViewImageAdapter(MainActivity.this, mTweetList, mColumnWidth);
		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				Intent i = new Intent(MainActivity.this, ImageActivity.class);
				TweetImage tweet = (TweetImage) mTweetList.get(position);
				i.putExtra("image", tweet.getImagePath());
				i.putExtra("text",tweet.getTweet());
				startActivity(i);
			}
		});

		new RetrieveImagesTask().execute();
	}

	protected void InitilizeGridLayout() {
		Resources r = getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, AppConstants.GRID_PADDING, r.getDisplayMetrics());
		mColumnWidth = (int) ((mUtils.getScreenWidth() - ((5) * padding)) / 2);
		mGridView.setColumnWidth(mColumnWidth);
	}

	class RetrieveImagesTask extends AsyncTask<Void, Integer, Void> {

		boolean err = false;
		
		@Override
		protected Void doInBackground(Void... urls) {
			PopularQuery p = new PopularQuery();
			try {
				List<TweetImage> lTweetImage = p.search(AppConstants.IMG_COUNT);
				for (int i = 0; i < lTweetImage.size(); i++) {
					publishProgress(i + 1);

					TweetImage tweetImage  = lTweetImage.get(i);
					File file = downloadFile(new URL(tweetImage.getImageUrl()), mDataPath + "/" + i + "_" + FilenameUtils.getName(tweetImage.getImageUrl()));

					tweetImage.setImagePath(file.getAbsolutePath());
					mTweetList.add(tweetImage);
				}
			} catch (TwitterException e) {
				Log.e(TAG, "Error getting tweets");
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			txtProgress.setText("Image " + progress[0] + " of " + AppConstants.IMG_COUNT);
			mAdapter.notifyDataSetChanged();
		}

		@Override
		protected void onPostExecute(Void result) {
			txtInfo.setVisibility(View.GONE);
			txtProgress.setVisibility(View.GONE);
			mAdapter.notifyDataSetChanged();
			if (err) {
				txtInfo.setVisibility(View.VISIBLE);
				txtInfo.setText("Error downloading images.");
				Toast.makeText(MainActivity.this, "It was not possible to download the images, " + "please check your internet connection.", Toast.LENGTH_SHORT).show();
			}
		}
		
		protected File downloadFile(URL url, String dest) throws IOException {
			File file = new File(dest);
			URLConnection conection = url.openConnection();
			conection.connect();
			InputStream input = new BufferedInputStream(url.openStream(), 8192);
			OutputStream output = new FileOutputStream(file);
			byte data[] = new byte[1024];
			int count = 0;
			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}
			output.flush();
			output.close();
			input.close();
			return file;
		}
	}
}